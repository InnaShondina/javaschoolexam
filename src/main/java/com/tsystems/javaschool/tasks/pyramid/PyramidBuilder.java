package com.tsystems.javaschool.tasks.pyramid;

import java.util.Comparator;
import java.util.List;

public class PyramidBuilder {

    private int col;

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException{
        try {
            int size = inputNumbers.size();
            if (((Math.sqrt(8*size + 1)-1)/2)%1 == 0) {
                int row = getLevel(size);
                int column = 2*row-1;
                int lvl;
                int pyramidArray [][] = new int[row][column];

                inputNumbers.sort(Comparator.naturalOrder());
                int oldLvl = 0;
                int countLvl = 0;
                for (int i = 0; i < size; i++) {
                    lvl = getLevel(i);
                    if(lvl!=oldLvl) countLvl = 0;
                    pyramidArray[lvl][row-lvl-1 +countLvl*2] = inputNumbers.get(i);
                    oldLvl = lvl;
                    countLvl++;
                }
                return pyramidArray;
            } else {
                throw new CannotBuildPyramidException("Input numbers cannot build pyramid");
            }

        }catch (Exception e){
            throw new CannotBuildPyramidException("Input numbers cannot build pyramid");
        }
    }

    private int getLevel(int number){
        return (int)Math.floor((Math.sqrt(8*number + 1)-1)/2);
    }

}
