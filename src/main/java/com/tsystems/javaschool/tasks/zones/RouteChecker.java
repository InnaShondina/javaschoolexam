package com.tsystems.javaschool.tasks.zones;

import java.util.List;

public class RouteChecker {

    int routeMatrix[][];
    /**
     * Checks whether required zones are connected with each other.
     * By connected we mean that there is a path from any zone to any zone from the requested list.
     *
     * Each zone from initial state may contain a list of it's neighbours. The link is defined as unidirectional,
     * but can be used as bidirectional.
     * For instance, if zone A is connected with B either:
     *  - A has link to B
     *  - OR B has a link to A
     *  - OR both of them have a link to each other
     *
     * @param zoneState current list of all available zones
     * @param requestedZoneIds zone IDs from request
     * @return true of zones are connected, false otherwise
     */
    public boolean checkRoute(List<Zone> zoneState, List<Integer> requestedZoneIds){
        boolean result = false;
        int count = zoneState.size();
        int countRoute = requestedZoneIds.size();
        routeMatrix = new int[count][count];
        for (Zone zone:zoneState) {
            for (int neighbour:zone.getNeighbours()) {
                routeMatrix[zone.getId()-1][neighbour-1] = 1;
                routeMatrix[neighbour-1][zone.getId()-1] = 1;

            }
        }

        return makeRoute(requestedZoneIds);
    }
    private boolean makeRoute(List<Integer> requestedZoneIds){
        boolean haveNeighbours = false;
        for (int i = 0; i < requestedZoneIds.size() - 1; i++) {
            int from = requestedZoneIds.get(i);
            int to = requestedZoneIds.get(i + 1);
            if(!isNeighbour(from, to)){
                haveNeighbours = false;
                for(int j = i+1; j>=0; j--){
                    haveNeighbours = isNeighbour(to, requestedZoneIds.get(j));
                }
                if(!haveNeighbours) return false;

            }
            else haveNeighbours = true;
        }
        return haveNeighbours;
    }
    private boolean isNeighbour(int id1, int id2){
        if ( routeMatrix[id1 - 1][id2 - 1]==1) return true;
        else return false;
    }

}
